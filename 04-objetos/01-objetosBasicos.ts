let flash:{nombre:string, edad:number, poderes:string[], getNombre:()=>string } = { //luego del nombre del objeto, declaro los tipos de las propiedades
    nombre: "Barry Allem",
    // edad: "24", //no dejaría agregar edad como un string
    edad:24,
    poderes: ["Correr muy rápido", "viajar en el tiempo"],
    getNombre(){ 
        return this.nombre;
    }
};

// flash = { //mientras se cumplan las props, se puede sobrescribir
//     nombre: "Clark Kent",
//     edad: 90,
//     poderes: ["puede volar"]
// };

// flash.padres = [] //no deja agregar una prop por fuera de la definición

// flash = {}; //esto tampoco se puede

flash.getNombre()

let superman2:{nombre:string, edad:number, poderes:any[], getNombre:()=>string } = { //si quiero cambiar la definición debo hacerlo en muchos objetos, esto está mal
    nombre: "Clark Kent",   
    edad:500,
    poderes: ["puede volar", "super velocidad"],
    getNombre(){ 
        return this.nombre;
    }
};