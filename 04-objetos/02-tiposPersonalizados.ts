type Heroe = {
    nombre:string,
    edad:number,
    // poderes:string[], //da error en la propiedad poderes poeque no es un arreglo de string
    poderes:any[], //solucionamos cambiando el tipo
    getNombre:()=>string
}

let flash2:Heroe = { 
    nombre: "Barry Allem",
    edad:24,
    poderes: ["Correr muy rápido", "viajar en el tiempo", 23],
    getNombre(){ 
        return this.nombre;
    }
};

let superman3:Heroe = { 
    nombre: "Clark Kent",   
    edad:500,
    poderes: ["puede volar", "super velocidad", 233],
    getNombre(){ 
        return this.nombre;
    }
};