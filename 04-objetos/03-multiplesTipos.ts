type Heroe2 = {
    nombre:string,
    edad:number
}

// let loquesea: any = true;
let loquesea: string | number | Heroe2 = "Fernando"; //podemos decir que se alguno de estos tipos

loquesea = {
    nombre: "flash",
    edad: 56
};