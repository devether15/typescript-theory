function sumar(a, b) {
    return a + b;
}
function saludar(nombre) {
    return "I'm " + nombre;
}
function salvarMundo() {
    console.log("El mundo está salvado");
}
// let miFuncion: (a:number, b:number) => number; //hace match con sumar
// let miFuncion: (x:string) => string; //hace match con saludar
var miFuncion; //hace match con saludar
miFuncion = 10;
miFuncion = sumar;
console.log(miFuncion(5, 5));
miFuncion = saludar;
console.log(miFuncion("Pedro"));
miFuncion = salvarMundo;
miFuncion();
