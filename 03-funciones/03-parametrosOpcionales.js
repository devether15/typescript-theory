function nombreCompleto(nombre, apellido) {
    if (apellido) {
        return nombre + ' ' + apellido;
    }
    else {
        return nombre;
    }
}
var imprimir = nombreCompleto("Joseph", "Elric");
console.log(imprimir);
