function nombreCompleto( nombre:string, apellido:string ):string { //al definirlos así los parámetros son obligatorios,
    return nombre + ' ' + apellido;                                // también pueden ser de tipo any, boolean, number, etc.
}                                                                  //P.D: El retorno también lo especificamos como string

let impresion = nombreCompleto("Clark","Kent"); //es obligatorio pasar los parámetros en la invocación de la función.

console.log(impresion);