function nombreCompleto(nombre, apellido) {
    return nombre + ' ' + apellido; // también pueden ser de tipo any, boolean, number, etc.
} //P.D: El retorno también lo especificamos como string
var impresion = nombreCompleto("Clark", "Kent"); //es obligatorio pasar los parámetros en la invocación de la función.
console.log(impresion);
