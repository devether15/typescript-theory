function nombreCompleto( nombre:string, apellido?:string ):string { //con el signo ? defino la opcionalidad
    if(apellido){
        return nombre + ' ' + apellido;
    }else{
        return nombre;
    }
}

let imprimir = nombreCompleto("Joseph", "Elric");

console.log(imprimir);