function nombreCompleto(nombre, apellido, capitalizado) {
    if (capitalizado === void 0) { capitalizado = false; }
    console.log(capitalizado);
    if (capitalizado) {
        return capitalizar(nombre) + ' ' + capitalizar(apellido);
    }
    else {
        return nombre + ' ' + apellido;
    }
}
function capitalizar(palabra) {
    return palabra.charAt(0).toUpperCase() + palabra.substr(1).toLocaleLowerCase();
}
var impresion = nombreCompleto("clark", "kent", true); //si no paso el parametro queda ne minuscula por defecto, si paso un true capitaliza
console.log(impresion);
