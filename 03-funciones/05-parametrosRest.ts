function nombreCompleto ( nombre:string, ...losDemasParametros:string[] ):string { //el operador REST ...indica que el resto de parametros de añadiran a un arreglo de srting
    return nombre + ' ' + losDemasParametros.join(" ");//el join permite concatenar elementos de un array
}

let superman:string = nombreCompleto("Clark", "Joseph", "Kent");
let ironman:string = nombreCompleto("Anthony", "Edward", "Tony", "Stark");

console.log(superman);
console.log(ironman);