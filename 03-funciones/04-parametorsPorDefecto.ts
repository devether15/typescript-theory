function nombreCompleto( nombre:string, apellido:string, capitalizado:boolean = false ):string { 

    console.log(capitalizado);

    if(capitalizado){
        return capitalizar(nombre) + ' ' + capitalizar(apellido);
    }else{
        return nombre + ' ' + apellido;
    }
}  

function capitalizar(palabra:string):string{
    return palabra.charAt(0).toUpperCase() + palabra.substr(1).toLocaleLowerCase();
}

let impresion = nombreCompleto("clark","kent", true); //si no paso el parametro queda ne minuscula por defecto, si paso un true capitaliza

console.log(impresion);