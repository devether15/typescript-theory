//LET VS VAR
let nombre = "Tony";
// let nombre = "Ricardo"

if (true) {
    let nombre = "Bruce";
}

console.log(nombre);


//CONSTANTES

const animal = "gato";
// const animal = "perro"; //  no se puede redeclarar la constante

const OPCIONES = {
    estado:true,
    audio:10,
    ultima: "main"
};

OPCIONES.estado= false;
OPCIONES.audio = 1;
console.log(OPCIONES);

//TEMPLATE LITERALS

let name1:string = "Bruce";
let name2:string = "Ricardo";

function getNombres():string{
    return `${name1} ${name2}`;
}

let mensaje3:string = `1. Esta es una linea normal
2. Hola ${ name1 }
3. Robin es: ${name2}
4. Los nombre son: ${ getNombres()}
5. ${5 + 9}
`;

console.log(mensaje3);
