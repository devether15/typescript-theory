let thor = {
    nombre: "thor",
    arma: "martillo"
};

let ironman = {
    nombre: "Ironman",
    arma: "ArmorSuit"
};

let capitan = {
    nombre: "Capitan América",
    arma: "Escudo"
};

let avengers = [thor, ironman, capitan];

//es5
for (let i = 0; i < avengers.length; i++) {   
    let avenger = avengers[i];
   console.log(avenger.nombre, avenger.arma);
}
console.log("===============================");

//for in
for (let i in avengers) {
   let avenger = avengers[i];
   console.log(avenger.nombre, avenger.arma);
}
console.log("===============================");


//ES6 for of
for(let avenger of avengers){
    console.log(avenger.nombre, avenger.arma);
}

