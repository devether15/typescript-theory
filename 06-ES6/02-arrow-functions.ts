// function sumar(a,b){
//     return a + b;
// }

sumar = (a,b) => a+b //equivalente en arrow function

console.log( sumar(2,2) );


//ejemplo2

// function darOrden_hulk (orden) {
//     return `Hulk ${orden}`;
// }

let darOrden_hulk = (orden) => `Hulk ${orden}`

console.log(darOrden_hulk("smassh!"));

//ejemplo3 
let capitan_america = {
    nombre: "Hulk",
    darOrden_hulk:function(){
        // //version 1 (funcion normal)
        // console.log("=====================");
        // let that = this;
        // setTimeout(function(){
        //     console.log(that.nombre + " Smassh!");
        //     console.log(this);
        //     console.log(that);
        // },1000)

        // //version 2 (arrow function 1)       
        // setTimeout(()=>{
        //     console.log(this.nombre + " Smassh!"); //con la funcion de felcha el ambito sigue siendo el ambito anterior (el objeto capitan_america) y no el objeto global
        // },1000) 

        //version 3 (arrow function simplificada)    
        setTimeout(()=>console.log(this.nombre + " Smassh!"), 1000);
    }
};

capitan_america.darOrden_hulk();
