var thor = {
    nombre: "thor",
    arma: "martillo"
};
var ironman = {
    nombre: "Ironman",
    arma: "ArmorSuit"
};
var capitan = {
    nombre: "Capitan América",
    arma: "Escudo"
};
var avengers = [thor, ironman, capitan];
//es5
for (var i = 0; i < avengers.length; i++) {
    var avenger = avengers[i];
    console.log(avenger.nombre, avenger.arma);
}
console.log("===============================");
//for in
for (var i in avengers) {
    var avenger = avengers[i];
    console.log(avenger.nombre, avenger.arma);
}
console.log("===============================");
//ES6 for of
for (var _i = 0, avengers_1 = avengers; _i < avengers_1.length; _i++) {
    var avenger = avengers_1[_i];
    console.log(avenger.nombre, avenger.arma);
}
