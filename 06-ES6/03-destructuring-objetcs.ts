let avengers = {
    nick: "Samuel Jackson",
    ironman: "Robert Downey Jr",
    vision: "Paul Bettany"
};

//si quiero extraer la data del objeto de forma tradicional:
// let nick = avengers.nick;
// let ironman = avengers.ironman;
// let vision = avengers.vision;

//en Es6 con una sola linea:
let {nick, ironman:warmachine, vision, thor ="Paul Walker" } = avengers; //crea las variables, en el caso de warmachine se usa : para reasignar el nombre, y se puede definir una nueva propiedad poro defecto (thor)

console.log(nick);
console.log(warmachine);
console.log(vision);
console.log(thor);