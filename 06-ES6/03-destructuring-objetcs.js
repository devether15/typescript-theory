var avengers = {
    nick: "Samuel Jackson",
    ironman: "Robert Downey Jr",
    vision: "Paul Bettany"
};
//si quiero extraer la data del objeto de forma tradicional:
// let nick = avengers.nick;
// let ironman = avengers.ironman;
// let vision = avengers.vision;
//en Es6 con una sola linea:
var nick = avengers.nick, warmachine = avengers.ironman, vision = avengers.vision, _a = avengers.thor, thor = _a === void 0 ? "Paul Walker" : _a; //crea las variables, en el caso de warmachine se usa : para reasignar el nombre, y se puede definir una nueva propiedad poro defecto (thor)
console.log(nick);
console.log(warmachine);
console.log(vision);
console.log(thor);
