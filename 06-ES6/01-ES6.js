//LET VS VAR
var nombre = "Tony";
// let nombre = "Ricardo"
if (true) {
    var nombre_1 = "Bruce";
}
console.log(nombre);
//CONSTANTES
var animal = "gato";
// const animal = "perro"; //  no se puede redeclarar la constante
var OPCIONES = {
    estado: true,
    audio: 10,
    ultima: "main"
};
OPCIONES.estado = false;
OPCIONES.audio = 1;
console.log(OPCIONES);
//TEMPLATE LITERALS
var name1 = "Bruce";
var name2 = "Ricardo";
function getNombres() {
    return name1 + " " + name2;
}
var mensaje3 = "1. Esta es una linea normal\n2. Hola " + name1 + "\n3. Robin es: " + name2 + "\n4. Los nombre son: " + getNombres() + "\n5. " + (5 + 9) + "\n";
console.log(mensaje3);
