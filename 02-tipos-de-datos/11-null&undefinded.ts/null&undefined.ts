let nada:undefined = undefined;

nada = "Hola mundo"; //esto no se permite, no es asignable un string a un tipo undefined
nada = null;

let ironman:string;

ironman = "Tony";
ironman = undefined; //no se puede saignar un undefined porque es un string (activando el strictNullChecks en el tscongig.json)

