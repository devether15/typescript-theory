var avengers = 5;
var villanos; //podemos hacer que ts advierta al nodefinir una variable
var otros = 2;
if (avengers > villanos) { // OUT=> Variable 'villanos' is used before being assigned.
    console.log('estamos salvados');
}
else {
    console.log('estamos muertos!');
}
//solo acepta tipos numéricos
otros = 123;
otros = 123.12;
otros = "123"; //OUT=> error TS2322: Type '"123"' is not assignable to type 'number'.
