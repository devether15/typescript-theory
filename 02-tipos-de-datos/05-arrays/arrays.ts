let arreglo:number[] = [1,2,3,4,5,6,7];
//arreglo.push("123")//OUT => error TS2345: Argument of type '"123"' is not assignable to parameter of type 'number'.

let enemigos:string[] = ["venom", "guason", "duende verde"]
//enemigos.push([12]); //si no es un arreglo de string da error OUT => error TS2345: Argument of type 'number[]' ierror TS2345: Argument of type 'number[]' is not assignable to parameter of type 'string'.

//enemigos. ya detecta el arreglo y muestras sugerencias de autocompletado
console.log(enemigos[0].charAt(0));