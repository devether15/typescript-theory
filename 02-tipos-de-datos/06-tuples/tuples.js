var heroe = ["Dr. Strange", 100]; //sino se agrega el boolean declarado en poscicion dos lanza un error OUT=> error TS2741: Property '2' is missing in type '[string, number]' but required in type '[string, number, boolean]'.
heroe[0] = 123; //da error porque el primer elemento de la tupla debe ser un string OUT=>error TS2322: Type '123' is not assignable to type 'string'.
heroe[1] = "123"; //da error porque el segundo elemento de la tupla debe ser un number OUT=>error TS2322: Type '"123"' is not assignable to type 'number'
