var Avenger = /** @class */ (function () {
    function Avenger(nombre, equipo, nombreReal) {
        this.nombre = "Sin nombre!"; //si no es especifica la privacidad, pro defectoes public
        this.equipo = undefined;
        this.nombreReal = undefined;
        this.puedePelear = false;
        this.peleasGanadas = 0;
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
    }
    Avenger.prototype.bio = function () {
        var mensaje = this.nombre + " " + this.nombreReal + " " + this.equipo;
        console.log(mensaje);
    };
    Avenger.prototype.cambiarEquipoPublico = function (nuevoEquipo) {
        return this.cambiarEquipo(nuevoEquipo);
    };
    Avenger.prototype.cambiarEquipo = function (nuevoEquipo) {
        if (nuevoEquipo === this.equipo) {
            return false;
        }
        else {
            this.equipo = nuevoEquipo;
            return true;
        }
    };
    return Avenger;
}());
var antman = new Avenger("Antman", "Cap", "Scott Lang");
antman.nombre = "Nick fury"; //esto lo permite porque la propiedad es publica
antman.equipo = "Ironman"; //esto compila pero para ts, solo debería ser accesible desde la clase o subclase porque la propiedad es protected
// console.log(antman);
antman.bio(); //el vscode me sugiere el metodo bio() cuanto tipeo antman.
console.log(antman.cambiarEquipoPublico("cap"));
