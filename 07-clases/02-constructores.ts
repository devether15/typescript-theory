class Avenger {
    nombre:string = "Sin nombre!"; //al hacer este hardcodeo es la unica que aparece por tener un valor (faltaria el constructor)
    equipo:string =undefined;
    nombreReal:string = undefined;
    puedePelear:boolean = false;
    peleasGanadas:number = 0;

    constructor( nombre:string, equipo:string, nombreReal:string ){ //el constructor sobrescribe las varibales por defecto
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
    }
}

let antman:Avenger = new Avenger("Antman", "Cap", "Scott Lan");
console.log(antman);
