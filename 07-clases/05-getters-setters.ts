class Avenger { 

    private _nombre:string;

    constructor( nombre?:string ){ 
        this._nombre = nombre;
    }

    get nombre():string{
        if ( this._nombre ){
            return this._nombre;
        }else{
            return "No tiene nombre asignado"
        }
    }

    set nombre(nombre:string){
        console.log("pasó por set nombre");

        if (nombre.length <= 3){
            console.error("El nombre debe ser mayor de tres caracteres");
            return;
        }
        this._nombre = nombre;
    }
}

let ciclope:Avenger = new Avenger("Wolverine");

console.log(ciclope.nombre);

ciclope.nombre = "lee"
console.log(ciclope.nombre);
