var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Mutantes = /** @class */ (function () {
    function Mutantes(nombre, nombreReal) {
        this.nombre = nombre;
        this.nombreReal = nombreReal;
    }
    return Mutantes;
}());
var wolverine = new Mutantes("Wolverine", "Logan"); // apesar de cumplir con los parametros dle constructor, marca error porque no se puede crear una instancia de una clase abstracta
//las clases abstractas sirven como moldespara otras clases.
//creo una clase que herede de Mutantes
var Xmen = /** @class */ (function (_super) {
    __extends(Xmen, _super);
    function Xmen() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Xmen;
}(Mutantes));
var wolverine3 = new Xmen("Wolverine", "Logan");
console.log(wolverine3); //imprime con la spropiedades provenientes de la clase abstracta, aun cuando la clase hija no tiene un constructor
