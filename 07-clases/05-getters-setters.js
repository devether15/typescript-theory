var Avenger = /** @class */ (function () {
    function Avenger(nombre) {
        this._nombre = nombre;
    }
    Object.defineProperty(Avenger.prototype, "nombre", {
        get: function () {
            if (this._nombre) {
                return this._nombre;
            }
            else {
                return "No tiene nombre asignado";
            }
        },
        set: function (nombre) {
            console.log("pasó por set nombre");
            if (nombre.length <= 3) {
                console.error("El nombre debe ser mayor de tres caracteres");
                return;
            }
            this._nombre = nombre;
        },
        enumerable: true,
        configurable: true
    });
    return Avenger;
}());
var ciclope = new Avenger("Wolverine");
console.log(ciclope.nombre);
ciclope.nombre = "lee";
console.log(ciclope.nombre);
