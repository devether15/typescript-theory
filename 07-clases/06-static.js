var Xmen = /** @class */ (function () {
    function Xmen() {
    }
    Xmen.crearXmen = function () {
        console.log("se creó usando un metodo estatico");
        return new Xmen();
    };
    Xmen.nombre = "wolverine";
    return Xmen;
}());
var wolverine2 = Xmen.crearXmen(); //los métodos estáticos no necesitans er instanciados, se pueden llamar directamente desde la clase
console.log(wolverine2);
