class Avenger {

    public nombre:string = "Sin nombre!"; //si no es especifica la privacidad, pro defectoes public
    protected equipo:string =undefined;
    private nombreReal:string = undefined;
    private puedePelear:boolean = false;
    private peleasGanadas:number = 0;

    constructor( nombre:string, equipo:string, nombreReal:string ){ 
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
    }

    public bio ():void{//si no coloco nada antes de la funcion por defecto es public,e s buena practica declaralo igualmente para readability 
        let mensaje:string = `${this.nombre} ${this.nombreReal} ${this.equipo}`;
        console.log(mensaje);
    }

    public cambiarEquipoPublico(nuevoEquipo:string):boolean{
        return this.cambiarEquipo(nuevoEquipo);
    }

    private cambiarEquipo(nuevoEquipo:string):boolean{//un método privado solo puede ser accedido desde la propia clase o un metodo publico de la clase
        if( nuevoEquipo === this.equipo ){
            return false;
        }else{
            this.equipo = nuevoEquipo;
            return true;
        }
    }
}

let antman:Avenger = new Avenger("Antman", "Cap", "Scott Lang");

antman.nombre = "Nick fury"; //esto lo permite porque la propiedad es publica

antman.equipo = "Ironman"; //esto compila pero para ts, solo debería ser accesible desde la clase o subclase porque la propiedad es protected

// console.log(antman);
antman.bio(); //el vscode me sugiere el metodo bio() cuanto tipeo antman.
console.log(antman.cambiarEquipoPublico("cap")); 