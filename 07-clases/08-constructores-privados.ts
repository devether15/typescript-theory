class Apocalipsis{
    static instancia:Apocalipsis;

    private constructor(public nombre:string){

    }

    static llamarApocalipsis(){

        if(!Apocalipsis.instancia){
            Apocalipsis.instancia = new Apocalipsis("Soy apocalipsis (true)")
        }

        return Apocalipsis.instancia;
    }
}

let apocalipsisFalso = new Apocalipsis("Soy apocalipsis(falso)"); //no permite hacer esto porque se trata de un constructor privado

let real = Apocalipsis.llamarApocalipsis();
console.log(real);
