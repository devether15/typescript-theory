class Avenger {

        // public nombre:string;     
    // public nombreReal:string;

    // constructor( nombre:string,  nombreReal:string ){ 
    //     this.nombre = nombre;
    //     this.nombreReal = nombreReal;
    // }  
    
    //todo el bloque anterior se puede simplificar de la siguiente forma: 
    constructor( public nombre:string, private nombreReal:string ){ 
        console.log("constructor Avenger llamado!"); //luego de llamar al constructor de la propia clase llama al padre
    }  
    
    protected getNombre(){
        console.log("getNombre Avenger (protected)");
        return this.nombre;
    }
}

class Xmen extends Avenger{
    constructor(a:string, b:string){
        console.log("constructor Xmen llamado!"); //se ejecuta primero
        super(a,b);
    }

    public getNombre():string{
        console.log("getNombre Xmen (publico)");
        return super.getNombre();
    }
}

// let ciclope:Avenger = new Avenger("Ciclope", "Scott");

//cambiams el tipo a Xmen y deberia tener las propiedades nombre y nombreReal heredadas de la clase Avenger
let ciclope:Xmen = new Xmen("Ciclope", "Scott"); //sino declaro el super en la clase hija, me lanza error

// console.log(ciclope);
console.log(ciclope.getNombre()); //púedo llamar un método de la clase padre (si es public), en el caso de protected desde un metodo de la clase