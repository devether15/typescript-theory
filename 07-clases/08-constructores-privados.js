var Apocalipsis = /** @class */ (function () {
    function Apocalipsis(nombre) {
        this.nombre = nombre;
    }
    Apocalipsis.llamarApocalipsis = function () {
        if (!Apocalipsis.instancia) {
            Apocalipsis.instancia = new Apocalipsis("Soy apocalipsis (true)");
        }
        return Apocalipsis.instancia;
    };
    return Apocalipsis;
}());
var apocalipsisFalso = new Apocalipsis("Soy apocalipsis(falso)"); //no permite hacer esto porque se trata de un constructor privado
var real = Apocalipsis.llamarApocalipsis();
console.log(real);
