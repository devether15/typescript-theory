class Xmen{

    static nombre:string = "wolverine";

    constructor(){

    }

    static crearXmen(){
        console.log("se creó usando un metodo estatico");
        return new Xmen();
    }
}

let wolverine2 = Xmen.crearXmen();//los métodos estáticos no necesitans er instanciados, se pueden llamar directamente desde la clase
console.log(wolverine2);