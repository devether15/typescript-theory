abstract class Mutantes{  // X-MEN!

    constructor(public nombre:string, public nombreReal:string){

    }

}

let wolverine = new Mutantes("Wolverine", "Logan"); // apesar de cumplir con los parametros dle constructor, marca error porque no se puede crear una instancia de una clase abstracta

//las clases abstractas sirven como moldespara otras clases.

//creo una clase que herede de Mutantes
class Xmen extends Mutantes {

}

let wolverine3 = new Xmen("Wolverine", "Logan");
console.log(wolverine3); //imprime con la spropiedades provenientes de la clase abstracta, aun cuando la clase hija no tiene un constructor