var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Avenger = /** @class */ (function () {
    // public nombre:string;     
    // public nombreReal:string;
    // constructor( nombre:string,  nombreReal:string ){ 
    //     this.nombre = nombre;
    //     this.nombreReal = nombreReal;
    // }  
    //todo el bloque anterior se puede simplificar de la siguiente forma: 
    function Avenger(nombre, nombreReal) {
        this.nombre = nombre;
        this.nombreReal = nombreReal;
        console.log("constructor Avenger llamado!"); //luego de llamar al constructor de la propia clase llama al padre
    }
    Avenger.prototype.getNombre = function () {
        console.log("getNombre Avenger (protected)");
        return this.nombre;
    };
    return Avenger;
}());
var Xmen = /** @class */ (function (_super) {
    __extends(Xmen, _super);
    function Xmen(a, b) {
        var _this = this;
        console.log("constructor Xmen llamado!"); //se ejecuta primero
        _this = _super.call(this, a, b) || this;
        return _this;
    }
    Xmen.prototype.getNombre = function () {
        console.log("getNombre Xmen (publico)");
        return _super.prototype.getNombre.call(this);
    };
    return Xmen;
}(Avenger));
// let ciclope:Avenger = new Avenger("Ciclope", "Scott");
//cambiams el tipo a Xmen y deberia tener las propiedades nombre y nombreReal heredadas de la clase Avenger
var ciclope = new Xmen("Ciclope", "Scott"); //sino declaro el super en la clase hija, me lanza error
// console.log(ciclope);
console.log(ciclope.getNombre()); //púedo llamar un método de la clase padre (si es public), en el caso de protected desde un metodo de la clase
