// function regresar( arg:any){
//     return arg;
// }

function regresar<T>( arg:T){ //es una convencion usar <T>, esto hace que la funcion se adapte al tipo de dati entrante
    return arg;
}

console.log(  regresar( 15.456789 ).toFixed(2));
console.log(  regresar( "Ricardo Tapia" ).charAt(0));
console.log(  regresar( new Date().getDay() ));