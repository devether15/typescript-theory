function functionGenerica<T> (arg:T){
    return arg;
};

type Heroe5 = {
    nombre:string;
    nombreReal:string;
}

type Villano5 = {
    nombre:string;
    poder:string;
}

let deadpool = {
    nombre:"Deadpool",
    nombreReal: "Wade Winston Wilson",
    poder: "regeneracion"
}

console.log( functionGenerica<Heroe5>(deadpool));