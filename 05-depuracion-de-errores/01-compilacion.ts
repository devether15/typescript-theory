/*
    esto es un comentario 
    multilinea
*/

//heroe es el personaje principal
let heroe3:string = "Ricardo Tapia (Robin)";
//contiene la edad
let edad:number = 30;

let mensaje2 = impirimir(heroe3,edad);
console.log(mensaje2);

//una funcion para imprimir
function impirimir(heroe3:string, edad:number):string{
    heroe3 = heroe3.toLowerCase();
    edad = edad + 10;

    return heroe3 + " " + edad;
}