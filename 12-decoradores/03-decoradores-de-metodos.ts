function editable( esEditable:boolean){
    return function( target:any, nombrePropeidad:string, descriptor:PropertyDescriptor){
        if (!esEditable){
            descriptor.writable = esEditable;
        }
    }
}

class Villano{

    constructor (public nombre: string){}

    @editable(false)
    plan(){
        console.log("Es dominar al mundo");
    }
}

let lex = new Villano("Lex Luthor");

lex.plan = function(){
    console.log("El plan es cortar flores");
}

lex.plan()
