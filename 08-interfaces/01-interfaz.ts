interface Xmen{
    nombre:string;
    poder?:string; //con el ? hacemos que sea opcional
}

function enviarMision(xmen: Xmen){
    console.log("Enviando a: " +xmen.nombre);
};

function enviarCuartel(xmen: Xmen){
    console.log("Regresando a cuartel: " +xmen.nombre + ", su poder es: " +xmen.poder);
};

let guepardo:Xmen = {
    // nombreXmen: "Wolverine", //si el nombre cambia, no cumple con la interface
    nombre: "Wolverine",
    // poder: "Regeneración" //si el dato poder no viene, nos da error
}

enviarMision(guepardo);
enviarCuartel(guepardo);
