function enviarMision(xmen) {
    console.log("Enviando a: " + xmen.nombre);
}
;
function enviarCuartel(xmen) {
    console.log("Regresando a cuartel: " + xmen.nombre + ", su poder es: " + xmen.poder);
}
;
var guepardo = {
    // nombreXmen: "Wolverine", //si el nombre cambia, no cumple con la interface
    nombre: "Wolverine"
};
enviarMision(guepardo);
enviarCuartel(guepardo);
