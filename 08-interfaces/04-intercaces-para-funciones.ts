interface DosNumerosFunction{
    (n1: number, num2:number) : number
};

let suma:DosNumerosFunction;

suma = function(a:number, b:number){
    return a + b;//si no cumplo con el retorno, marca error porque no cumplo con la salida esperada definida ne la interface.
};

let restar:DosNumerosFunction;

restar = function(numero1:string, numero2:number){ //si se cambia el tipo a string, también falla ya que la interface define que es un number
    return numero1 - numero2;
}



